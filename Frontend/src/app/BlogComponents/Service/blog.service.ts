import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Post, ResponseComment, ResponsePost  ,Comment} from '../model/post';
const link_api="http://127.0.0.1:8000/api/postapi/";
@Injectable({
  providedIn: 'root'
})
export class BlogService {

  constructor(private httpClient: HttpClient) { }
  getAll(): Observable<ResponsePost>{

    return this.httpClient.get<ResponsePost>(link_api)

    
  }
  AddComment(comment:Comment): Observable<Comment>{
    return this.httpClient.post<Comment>(link_api+"post_comment",comment) 
  }

  getComment(idpost:any): Observable<ResponseComment>{

    return this.httpClient.get<ResponseComment>(link_api+"post_comments/"+idpost)

    
  }
  
  

}

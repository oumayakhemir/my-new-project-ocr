export class Post{
    id :any;
    title :any;
    headerimage :any;
    author :any;
    content :any ;
    created_at :any;
    updated_at :any;
    category :any;
    likes :any;
    comments:Comment[]=[];

}
export class ResponsePost{
    status:any;
    payload:Post[];


}
export class Comment{
    id:any;
    date_added :any;
    body :any;
    post:any;
    
}
export class ResponseComment{
    status:any;
    payload:Comment[];


}
import { NgModule } from '@angular/core';
import {CommonModule, LocationStrategy, PathLocationStrategy} from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule } from '@angular/forms';
import {FileExplorerComponent} from './file-explorer.component';
import {NewFolderDialogComponent} from './modals/newFolderDialog/newFolderDialog.component';
import {RenameDialogComponent} from './modals/renameDialog/renameDialog.component';
import { NewFileDialogComponent } from './modals/new-file-dialog/new-file-dialog.component';
import {ImageCropperModule} from 'ngx-image-cropper';
import {PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarConfigInterface, PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import { DeleteDialogComponent } from './modals/delete-dialog/delete-dialog.component';
import {Ng2SearchPipeModule} from 'ng2-search-filter';
import { OpenImageComponent } from './modals/open-image/open-image.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
  suppressScrollY: true,
  wheelSpeed: 1,
  wheelPropagation: true,
  minScrollbarLength: 5
};

@NgModule({
    imports: [
        CommonModule,
        MatIconModule,
        MatDialogModule,
        MatInputModule,
        FormsModule,
        MatButtonModule,
        ImageCropperModule,
        PerfectScrollbarModule,
        Ng2SearchPipeModule

    ],
  declarations: [
    FileExplorerComponent,
    NewFolderDialogComponent,
    RenameDialogComponent,
    NewFileDialogComponent,
    DeleteDialogComponent,
    OpenImageComponent],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }
  ],
  exports: [FileExplorerComponent],
  entryComponents: [NewFolderDialogComponent, RenameDialogComponent]
})
export class FileExplorerModule {}

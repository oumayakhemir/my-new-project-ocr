import { Injectable } from '@angular/core';
import { v4 } from 'uuid';
import { FileElement } from '../model/element';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {IEnumerable, List} from 'linq-typescript';
const url = 'http://localhost:8000/fileExplorer/';

@Injectable({
  providedIn: 'root'
  })
export class FileService {

  tab: FileElement[] = [];
  private res: FileElement[] = [];
  private querySubject !: BehaviorSubject<FileElement[]>;
  private checked: boolean = false;
  private searchName: string = '';
  constructor(private http: HttpClient) {}

  getFiles (user_id: string): Observable<FileElement[]> {
    return this.http.get<FileElement[]>(url + 'get_files/' + user_id);
  }

  allFile (user_id: string, folderId: string) {
    this.getFiles(user_id).subscribe(
      data => {
        this.tab = data ;
        const result: FileElement[] = this.tab.filter(f => f.parent === folderId);
        this.querySubject.next(result);
      });
  }

  addFile(fileElement: FileElement): Observable<FileElement> {
    const formData = new FormData();
    formData.append('id', fileElement.id);
    formData.append('name', fileElement.name);
    formData.append('parent', fileElement.parent);
    formData.append('is_folder', fileElement.is_folder);
    formData.append('img', fileElement.img);
    return this.http.post<FileElement>(url + 'add_file/' + fileElement.user, formData);
  }

  add(fileElement: FileElement) {
    fileElement.id = v4();
    this.addFile(fileElement).subscribe(
      (data) => {
          fileElement.is_folder = data.is_folder;
          fileElement.img = data.img;
      }, () => {},
      () => {
        this.tab.push(fileElement);
        const result: FileElement[] = this.tab.filter(f => f.parent === fileElement.parent);
        this.querySubject.next(result);
      }
    );
    return fileElement;
  }

  deleteFiles (fileId: any) {
     this.http.delete<any>(url + 'delete_files/' + fileId).subscribe();
  }

  putFile (file: FileElement): Observable<FileElement> {
    return this.http.put<FileElement>(url + 'rename_file/' + file.id, file);
  }

  put(file: FileElement) {
    this.putFile(file).subscribe(
      data => {},
       () => {},
      () => {
        this.set_checked_file(false);
        let index = this.tab.indexOf(file);
        this.tab[index] = file;
        const result: FileElement[] = this.tab.filter(f => f.parent === file.parent);
        this.querySubject.next(result);
      });
  }

  serch(e: any, folderId: string) {
    const result: FileElement[] = this.tab.filter(f =>
      f.name.toLocaleLowerCase().includes(e.name.toLocaleLowerCase())
      && f.parent === folderId);
    this.querySubject.next(result);
  }

  TraverseTree(list: List<FileElement>, id: string): IEnumerable<FileElement> {
    return list.where(child => child.parent === id)
      .union(list.where(child => child.parent === id)
        .selectMany(parent => this.TraverseTree(list, parent.id)));
  }

  queryInFolder(folderId: string) {
    const result: FileElement[] = this.tab.filter(f => f.parent === folderId);
    if (!this.querySubject) {
      this.querySubject = new BehaviorSubject(result);
    } else {
      this.querySubject.next(result);
    }
    return this.querySubject.asObservable();
  }

  get(id: string): any {
    this.res = this.tab.filter(f => f.id === id);
    return this.res[0];
  }

  clone(element: FileElement) {
    return JSON.parse(JSON.stringify(element));
  }

  get table(): FileElement[] {
    return this.tab;
  }

  get List(): List<FileElement> {
    let list = new List<FileElement>();
    this.tab.forEach(item => {
      list.push(item);
    });
    return list;
  }

  get checked_file(): boolean {
    return this.checked;
  }

  set_checked_file(checked: boolean) {
    this.checked = checked;
  }

}

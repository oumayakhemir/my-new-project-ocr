import {animate, state, style, transition, trigger} from '@angular/animations';

export const toggleFade = trigger('fade', [
  state('void', style({transform: 'translateY(-100%)', opacity: 0})),
  transition(':enter,:leave', animate('200ms ease-in-out'))
]);

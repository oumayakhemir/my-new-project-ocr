import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {FileElement} from './model/element';
import {NewFolderDialogComponent} from './modals/newFolderDialog/newFolderDialog.component';
import {RenameDialogComponent} from './modals/renameDialog/renameDialog.component';
import {NewFileDialogComponent} from './modals/new-file-dialog/new-file-dialog.component';
import {FileBase64} from './model/fileBase64';
import {toggleFade} from './animation/toggle';
import {FileService} from './service/file.service';
import {List} from 'linq-typescript';
import {DeleteDialogComponent} from './modals/delete-dialog/delete-dialog.component';
import {OpenImageComponent} from './modals/open-image/open-image.component';

@Component({
  selector: 'file-explorer',
  templateUrl: './file-explorer.component.html',
  styleUrls: ['./file-explorer.component.scss'] ,
  animations: [toggleFade]
})
export class FileExplorerComponent implements OnInit {
  constructor(public dialog: MatDialog,
              public fileService: FileService
  ) {}

  @Input() fileElements !: FileElement[];
  @Input() canNavigateUp !: string | boolean;
  @Input() path !: string;

  @Output() folderAdded = new EventEmitter<{ name: string }>();
  @Output() fileAdded = new EventEmitter<{ file: FileBase64}>();
  @Output() delete = new EventEmitter<{files: FileElement[]}>();
  @Output() elementRenamed = new EventEmitter<{ selected: FileElement; rename: string }>();
  @Output() cutElement = new EventEmitter<{ selected: FileElement }>();
  @Output() nameSearch = new EventEmitter<{ name: any }>();
  @Output() navigatedDown = new EventEmitter<FileElement>();
  @Output() navigatedUp = new EventEmitter();
  @Output() pasteElement = new EventEmitter();
  @Output() downloadElement = new EventEmitter<{selected: FileElement}>();

  is_checked: Boolean = false;
  res !: List<FileElement>;
  result !: FileElement[];
  element_selected !: FileElement;
  search_in = '';
  searchName: any ='';

  ngOnInit() {
    this.search_in = 'search in' + ' Files';
  }
  openDeleteDialog() {
    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '400px'
    });
    dialogRef.afterClosed().subscribe(res => {
      this.delete.emit({ files: this.result });
      this.is_checked = false;
    });
  }
  navigate(element: FileElement) {
    if (element.is_folder) {
      console.log(element);
      this.search_in = 'search in ' + element.name;
      this.navigatedDown.emit(element);
    }
  }
  navigateUp() {
    this.navigatedUp.emit();
  }
  openNewFolderDialog() {
    let dialogRef = this.dialog.open(NewFolderDialogComponent, {
      width: '500px'
    });

    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.folderAdded.emit({ name: res });
      }
    });
  }
  openNewFileDialog() {
    let dialogRef = this.dialog.open(NewFileDialogComponent);
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.fileAdded.emit({ file: res });
      }
    });
  }
  openRenameDialog() {
    let dialogRef = this.dialog.open(RenameDialogComponent, {
      data : {
        oldName : this.element_selected.name
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.elementRenamed.emit({
          selected: this.element_selected,
          rename: res });
      }
    });
  }
  elementCut() {
    this.cutElement.emit({selected: this.element_selected});
  }
  elementPast() {
    this.pasteElement.emit();
  }
  isAllSelected(e: any, item: any) {
    this.fileService.table.forEach(val => {
      if (val.id === item.id) val.is_selected = !val.is_selected;
      else {
        val.is_selected = false;
      }
    });
    this.fileService.set_checked_file(item.is_selected);
    this.res = this.fileService.List;
    this.result = this.fileService.TraverseTree(this.res, item.id).toArray();
    this.result.push(item);
    this.element_selected = item;
  }
  search() {
    setTimeout(() =>
      {
        this.nameSearch.
        emit( {name: this.searchName});
      },
      2000);
  }
  openImage(image :any) {
    let dialogRef = this.dialog.open(OpenImageComponent, {
      data : {
        image: image
      }
    });
  }
  onDownload() {
    this.downloadElement.emit({selected: this.element_selected});
  }

}

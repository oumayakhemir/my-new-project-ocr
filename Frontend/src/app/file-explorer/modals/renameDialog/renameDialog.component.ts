import {Component, Inject, OnInit} from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-renameDialog',
  templateUrl: './renameDialog.component.html',
  styleUrls: ['./renameDialog.component.scss']
})
export class RenameDialogComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<RenameDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {}

  rename !: string;

  ngOnInit() {
    this.rename = this.data.oldName;
  }
}

import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {base64ToFile, ImageCroppedEvent, ImageTransform} from 'ngx-image-cropper';
import {FileBase64} from '../../model/fileBase64';
import * as FileSaver from "file-saver";


@Component({
  selector: 'app-new-file-dialog',
  templateUrl: './new-file-dialog.component.html',
  styleUrls: ['./new-file-dialog.component.scss']
})
export class NewFileDialogComponent {

  constructor(public dialogRef: MatDialogRef<NewFileDialogComponent>) { }
  url !: string;
  file: FileBase64 = new FileBase64();
  transform: ImageTransform = {};
  rotation = 0;
  scale = 1;
  translateH = 0;
  translateV = 0;
  canvasRotation = 0;
  loading = false;
  imgChangeEvt: any = '';
  cropImgPreview: any = '';
  originalFile !: any;

  onFileChange(event: any): void {
    this.imgChangeEvt = event;
    this.originalFile = event.target.files[0];
    this.resetImage();
  }
  cropImg(e: ImageCroppedEvent) {
    this.cropImgPreview = e.base64;
    this.file.base64 = this.cropImgPreview;
    this.file.name = this.originalFile.name.split('.',1)[0];
  }
  imgLoad() {

  }
  initCropper() {

  }
  imgFailed() {
    // error msg
  }
  resetImage() {
    this.scale = 1;
    this.rotation = 0;
    this.translateH = 0;
    this.translateV = 0;
    this.canvasRotation = 0;
    this.transform = {};
  }

  rotationToLeft() {
    this.rotation = this.rotation - 1;
    this.transform = {
      ...this.transform,
      rotate: this.rotation
    };
  }
  rotationToRight() {
    this.rotation = this.rotation + 1;
    this.transform = {
      ...this.transform,
      rotate: this.rotation
    };
  }
  zoomOut() {
    this.scale -= .1;
    this.transform = {
      ...this.transform,
      scale: this.scale
    };
  }
  zoomIn() {
    this.scale += .1;
    this.transform = {
      ...this.transform,
      scale: this.scale
    };
  }
  moveLeft() {
    this.transform = {
      ...this.transform,
      translateH: ++this.translateH
    };
  }
  moveRight() {
    this.transform = {
      ...this.transform,
      translateH: --this.translateH
    };
  }
  moveTop() {
    this.transform = {
      ...this.transform,
      translateV: ++this.translateV
    };
  }
  moveBottom() {
    this.transform = {
      ...this.transform,
      translateV: --this.translateV
    };
  }
  flipAfterRotate() {
    const flippedH = this.transform.flipH;
    const flippedV = this.transform.flipV;
    this.transform = {
      ...this.transform,
      flipH: flippedV,
      flipV: flippedH
    };
    this.translateH = 0;
    this.translateV = 0;
  }
  rotateLeft() {
    setTimeout(() => {
      this.canvasRotation--;
      this.flipAfterRotate();
    });
  }
  rotateRight() {
    setTimeout(() => {
      this.canvasRotation++;
      this.flipAfterRotate();
    });
  }
  flipHorizontal() {
    this.transform = {
      ...this.transform,
      flipH: !this.transform.flipH
    };
  }

  flipVertical() {
    this.transform = {
      ...this.transform,
      flipV: !this.transform.flipV
    };
  }













  //this.file = file;

  /* onSelectFile(event: Event) {
   const target = event.target as HTMLInputElement;
   const files = target.files as FileList;
   if (files) {
     const reader = new FileReader();
     reader.readAsDataURL(files[0]);
     this.file = files[0];
     reader.onload = (e: any) => {
       this.url = e.target.result;
     };
   }
 }*/

}

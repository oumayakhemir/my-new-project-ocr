import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-open-image',
  templateUrl: './open-image.component.html',
  styleUrls: ['./open-image.component.scss']
})
export class OpenImageComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<OpenImageComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  img !:any;
  ngOnInit(): void {
    this.img = this.data.image;
  }

}

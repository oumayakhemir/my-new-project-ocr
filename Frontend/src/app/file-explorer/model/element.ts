export class FileElement {
  id?: string |any;
  is_folder !: boolean|any;
  name !: string|any;
  parent !: string|any;
  img !: string|any;
  is_selected: boolean | null = false;
  user !: string | null;
}

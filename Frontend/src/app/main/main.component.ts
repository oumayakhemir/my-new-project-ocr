import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { BlogService } from '../BlogComponents/Service/blog.service';
import { Post ,Comment } from '../BlogComponents/model/post';



@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  articles:Post[]=[];
  body:any;
  comment=new Comment();
  comments:Comment[]=[];

  constructor(private blogservice:BlogService) { }

  ngOnInit(): void {
    this.blogservice.getAll().subscribe(
      (data)=>{this.articles=data.payload;this.articles.forEach(item=>this.blogservice.getComment(item.id).subscribe(data=>{item.comments=data.payload;}));
      ;console.log(this.articles);},
      error=>console.log(error),
      ()=>{
        //this.articles.forEach(item=>this.blogservice.getComment(item.id).subscribe(data=>{this.comments=data.payload;console.log(this.comments)}));
           });
    
    
  }
  addcomment(i:any,idpost:any){
    this.comment.body=this.body;
    this.comment.post=idpost;
    console.log(this.comment);
    this.blogservice.AddComment(this.comment).subscribe(()=>this.articles[i].comments.push(this.comment));

  }
  
  

}

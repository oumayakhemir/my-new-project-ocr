import {AfterViewInit, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {FileElement} from "../file-explorer/model/element";
import {FileService} from "../file-explorer/service/file.service";
import * as FileSaver from "file-saver";
import {base64ToFile} from "ngx-image-cropper";

@Component({
  selector: 'app-archive',
  templateUrl: './archive.component.html',
  styleUrls: ['./archive.component.scss']
})
/*export class ArchiveComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}*/
export class ArchiveComponent implements  AfterViewInit, OnInit {
  fileElements !: Observable<FileElement[]> ;
  currentRoot !: FileElement | any;
  currentPath !: string;
  canNavigateUp = false;
  toDelete !: FileElement[];
  cut_pasteElement !: FileElement;

  constructor(public fileService: FileService,
              private cdref: ChangeDetectorRef) {}
  get userLogin():any {
    return JSON.parse(localStorage.getItem('userLogin') !);
  }
  ngAfterViewInit() {
    this.fileService.allFile(this.userLogin.user_id,this.currentRoot ? this.currentRoot.id : 'root');
    this.updateFileElementQuery();
    this.cdref.detectChanges();
  }
  ngOnInit() {

  }
  addFolder(folder: { name: string }) {
    this.fileService.add({
      is_folder: 'True',
      name: folder.name,
      parent: this.currentRoot ? this.currentRoot.id : 'root',
      img : null,
      is_selected: null,
      user : this.userLogin.user_id });
    this.updateFileElementQuery();
  }
  addFile(file: any) {
    this.fileService.add({
      is_folder: 'False',
      name: file.file.name ,
      parent: this.currentRoot ? this.currentRoot.id : 'root',
      img : file.file.base64,
      is_selected: null,
      user :this.userLogin.user_id });
    this.updateFileElementQuery();
  }
  navigateToFolder(element: FileElement) {
    this.currentRoot = element;
    this.updateFileElementQuery();
    this.currentPath = this.pushToPath(this.currentPath, element.name);
    this.canNavigateUp = true;
  }
  navigateUp() {
    if (this.currentRoot && this.currentRoot.parent === 'root') {
      this.currentRoot = null;
      this.canNavigateUp = false;
      this.updateFileElementQuery();
    } else {
      this.currentRoot = this.fileService.get(this.currentRoot.parent);
      this.updateFileElementQuery();
    }
    this.currentPath = this.popFromPath(this.currentPath);

  }
  updateFileElementQuery() {
    this.fileElements = this.fileService.queryInFolder(this.currentRoot ? this.currentRoot.id : 'root');
  }
  pushToPath(path: string, folderName: string) {
    let p = path ? path : '';
    p += `${folderName}/`;
    return p;
  }
  popFromPath(path: string) {
    let p = path ? path : '';
    let split = p.split('/');
    split.splice(split.length - 2, 1);
    p = split.join('/');
    return p;
  }
  removeElements(e: any) {
    this.toDelete = e.files;
    this.toDelete.forEach((item) => {
      this.fileService.deleteFiles(item.id);
      const index = this.fileService.table.indexOf(item);
      if (index !== -1) {
        this.fileService.table.splice(index, 1);
      }
    });
    this.fileService.set_checked_file(false);
    this.fileService.List;
    this.updateFileElementQuery();
  }
  renameElement(e: any) {
    e.selected.name = e.rename;
    e.selected.is_selected = false;
    this.fileService.put(e.selected);
  }
  cutElement(e: any) {
    this.cut_pasteElement = e.selected;
  }
  pasteElement() {
    if(this.currentRoot === null) {
      this.cut_pasteElement.parent = 'root';
    }
    else {
      this.cut_pasteElement.parent = this.currentRoot.id;
    }

    this.cut_pasteElement.is_selected = false;
    this.fileService.put(this.cut_pasteElement);
  }
  search(e: any) {
    let folderId = this.currentRoot ? this.currentRoot.id : 'root';
    this.fileService.serch(e, folderId);
  }
  downloadFile(e: any){
    const fileBlob = base64ToFile(e.selected.img);
     const file: File = new File(
       [fileBlob],
       e.selected.name,
       {
         type : 'image/png'
       });
    FileSaver.saveAs(file);
  }


}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { AccountComponent } from './account/account.component';
import { AuthModule } from './auth/auth.module';
import { HttpClientModule } from '@angular/common/http';
import { ResetPasswordModule } from './UserComponent/reset-password/reset-password.module';
import { VerifyEmailComponent } from './UserComponent/verify-email/verify-email.component';
import { ChangePasswordModule } from './UserComponent/change-password/change-password.module';
import { NewPasswordModule } from './UserComponent/new-password/change-password.module';
import { ListDocumentsComponent } from './DocumentComponents/list-documents/list-documents.component';
import { ShowDocumentComponent } from './DocumentComponents/show-document/show-document.component';
import { EditDocumentComponent } from './DocumentComponents/edit-document/edit-document.component';
import { CreateDocumentModule } from './DocumentComponents/create-document/create-document.model';
import { MainComponent } from './main/main.component';
import { FooterComponent } from './footer/footer.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FrontTemplateComponent } from './front-template/front-template.component';
import { DocsComponent } from './docs/docs.component';
import { DocsModule } from './docs/docs.module';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { EditDocumentModule } from './DocumentComponents/edit-document/edit-document.model';
import { ArchiveComponent } from './archive/archive.component';
import {FileExplorerModule} from "./file-explorer/file-explorer.module";
import {FileService} from "./file-explorer/service/file.service";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {CommonModule} from "@angular/common";
import {MatDialogModule} from "@angular/material/dialog";
import {DeleteDialogComponent} from "./file-explorer/modals/delete-dialog/delete-dialog.component";
import {NewFolderDialogComponent} from "./file-explorer/modals/newFolderDialog/newFolderDialog.component";
import { AddArticleComponent } from './BlogComponents/add-article/add-article.component';
import { ListArticleComponent } from './BlogComponents/list-article/list-article.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AccountComponent,
    VerifyEmailComponent,
    ListDocumentsComponent,
    ShowDocumentComponent,
    MainComponent,
    FooterComponent,
    DashboardComponent,
    FrontTemplateComponent,
    ArchiveComponent,
    AddArticleComponent,
    ListArticleComponent,


  ],
  imports: [
    MatDialogModule,
    CommonModule,
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AuthModule,
    ResetPasswordModule,
    ChangePasswordModule,
    NewPasswordModule,
    CreateDocumentModule,
    DocsModule,
    SweetAlert2Module,
    SweetAlert2Module.forRoot(),
    SweetAlert2Module.forChild({ /* options */ }),
    EditDocumentModule,
    FileExplorerModule,
    FormsModule,

  ],

  providers: [
    FileService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

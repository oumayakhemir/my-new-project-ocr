import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from 'src/app/auth/auth.model';
import { DashboardComponent } from 'src/app/dashboard/dashboard.component';
import { DocService } from '../document.service';
import { Document } from '../document.model';

@Component({
  selector: 'app-show-document',
  templateUrl: './show-document.component.html',
  styleUrls: ['./show-document.component.css']
})
export class ShowDocumentComponent implements OnInit {
  error:string=null;
  success:string=null;
  user: User;
  userSub: Subscription;
  modelTypes = ["CIN","FACTURE","PASSPORT","MEDICAL_DOC"];
  selectedValue = "Select the Document Type";
  url: any; //Angular 11, for stricter type
	msg = "";
  img :string='';
  imgf :File;
  DJANGO_SERVER = 'http://127.0.0.1:8000'
  response;
  imageURL;
  doc : Document;
  doc_id:any;

  constructor(private docService: DocService ,private dash: DashboardComponent ,private _Activatedroute:ActivatedRoute) { }

  ngOnInit(): void {
    this._Activatedroute.paramMap.subscribe(params => { 
      this.doc_id = params.get("id"); 
    });
    this.user = this.dash.user ;
    this.docService.detailDoc(this.doc_id).subscribe((data:Document)=>{
      this.doc=data;
    });
  }

}

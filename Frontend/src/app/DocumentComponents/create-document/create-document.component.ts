import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { User } from 'src/app/auth/auth.model';
import { AuthService } from 'src/app/auth/auth.service';
import { DashboardComponent } from 'src/app/dashboard/dashboard.component';
import Swal from 'sweetalert2';
import { createDoc } from '../document.model';
import { DocService } from '../document.service';





@Component({
  selector: 'app-create-document',
  templateUrl: './create-document.component.html',
  styleUrls: ['./create-document.component.css']
})
export class CreateDocumentComponent implements OnInit {
 
  createForm: FormGroup;
  error:string=null;
  success:string=null;
  user: User;
  userSub: Subscription;
  modelTypes = ["CIN","FACTURE","PASSPORT","MEDICAL_DOC"];
  selectedValue = "Select the Document Type";
  url: any; //Angular 11, for stricter type
	msg = "";
  img :string='';
  imgf :File;
  DJANGO_SERVER = 'http://127.0.0.1:8000'
  response;
  imageURL;


  constructor(private docService: DocService ,private dash: DashboardComponent ,private formBuilder: FormBuilder) { }

  ngOnInit(){
    this.user = this.dash.user ;
    this.createForm = this.formBuilder.group({
      title: ['', [Validators.required, Validators.minLength(3)]],
      description: ['', [Validators.required, Validators.minLength(10)]],
      modelType: ['Select the Document Type', [Validators.required, Validators.minLength(3)]],
      owner: [this.user.email],
      docImage: [''],
      img: [null],

    });
  }

  onCreate(){
    console.log(this.createForm)
    
    const formData = new FormData();
    formData.append('title',this.createForm.get('title').value)
    formData.append('description',this.createForm.get('description').value)
    formData.append('modelType',this.createForm.get('modelType').value)
    formData.append('owner',this.user.user_id)
    formData.append('file', this.createForm.get('docImage').value)

    this.docService.upload(formData).subscribe(
      (res) => {
        Swal.fire({
          title: 'Document has Beed Added',
          text: "You won't be able to revert this!",
          icon: 'success',
          showCancelButton: false,
          confirmButtonColor: '#3085d6',
        })
        this.response = res;
        this.imageURL = `${this.DJANGO_SERVER}${res.file}`;
        console.log(res);
        console.log(this.imageURL);
      },
      (err) => {  
        console.log(err);
      }
    );
  }
  
  imagePreview(e) {
    const file = (e.target as HTMLInputElement).files[0];

    this.createForm.patchValue({
      img: file
    });

    this.createForm.get('img').updateValueAndValidity()

    const reader = new FileReader();
    reader.onload = () => {
      this.url = reader.result as string;
    }
    reader.readAsDataURL(file)
  }

  //selectFile(event) { //Angular 8
	selectFile(event :any) { //Angular 11, for stricter type
		if(!event.target.files[0] || event.target.files[0].length == 0) {
			this.msg = 'You must select an image';
			return;
		}
		
		var mimeType = event.target.files[0].type;
		
		if (mimeType.match(/image\/*/) == null) {
			this.msg = "Only images are supported";
			return;
		}
    const file = event.target.files[0];
    this.createForm.get('docImage').setValue(file);

		var reader = new FileReader();
		reader.readAsDataURL(event.target.files[0]);
		
		reader.onload = (_event) => {
			this.msg = "";
			this.url = reader.result;
      //this.img = ("http://127.0.0.1:8000/media/images/" + event.target.files[0].name);
    
		}

	}

  OnExtract()
  {
    console.log(this.createForm)
    
    const formData = new FormData();
    formData.append('modelType',this.createForm.get('modelType').value)
    formData.append('file', this.createForm.get('docImage').value)

    this.docService.upload(formData).subscribe(
      (res) => {
        Swal.fire({
          title: 'Document has Beed Added',
          text: "You won't be able to revert this!",
          icon: 'success',
          showCancelButton: false,
          confirmButtonColor: '#3085d6',
        })
        this.response = res;
        this.imageURL = `${this.DJANGO_SERVER}${res.file}`;
        console.log(res);
        console.log(this.imageURL);
      },
      (err) => {  
        console.log(err);
      }
    );

  }


  
}

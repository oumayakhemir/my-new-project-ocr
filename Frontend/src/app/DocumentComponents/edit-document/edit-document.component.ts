import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from 'src/app/auth/auth.model';
import { DashboardComponent } from 'src/app/dashboard/dashboard.component';
import Swal from 'sweetalert2';
import { DocService } from '../document.service';
import { Document } from '../document.model';

@Component({
  selector: 'app-edit-document',
  templateUrl: './edit-document.component.html',
  styleUrls: ['./edit-document.component.css']
})
export class EditDocumentComponent implements OnInit {

  editForm: FormGroup;
  error:string=null;
  success:string=null;
  user: User;
  userSub: Subscription;
  modelTypes = ["CIN","FACTURE","PASSPORT","MEDICAL_DOC"];
  selectedValue = "Select the Document Type";
  url: any; //Angular 11, for stricter type
	msg = "";
  img :string='';
  imgf :File;
  DJANGO_SERVER = 'http://127.0.0.1:8000'
  response;
  imageURL;
  doc : Document;
  doc_id:any;
  e:any;


  constructor(private docService: DocService ,private dash: DashboardComponent ,private _Activatedroute:ActivatedRoute) { }

  ngOnInit(){
    this._Activatedroute.paramMap.subscribe(params => { 
      this.doc_id = params.get("id"); 
    });
    this.user = this.dash.user ;
    this.docService.detailDoc(this.doc_id).subscribe((data:Document)=>{
      this.doc=data;
    });
    this.editForm = new FormGroup({
      'title' : new FormControl(''),
      'description': new FormControl('',Validators.required),
      'modelType': new FormControl('',[Validators.required,Validators.email]),
      'owner': new FormControl(this.user.email),
      'docImage':new FormControl(''),
      'img': new FormControl(),
     });
     
    }

  onCreate(){
    console.log(this.editForm)
    
    const formData = new FormData();
    formData.append('title',this.editForm.get('title').value)
    formData.append('description',this.editForm.get('description').value)
    formData.append('modelType',this.editForm.get('modelType').value)
    formData.append('owner',this.user.email)
    if(!this.url){
      
    }
    else
    {formData.append('file', this.editForm.get('docImage').value) }
    

    this.docService.EditDoc(formData,this.doc_id).subscribe(
      (res) => {
        Swal.fire({
          title: 'Document has Been Edited',
          text: "You won't be able to revert this!",
          icon: 'success',
          showCancelButton: false,
          confirmButtonColor: '#3085d6',
        })
        this.response = res;
        this.imageURL = `${this.DJANGO_SERVER}${res.file}`;
        console.log(res);
        console.log(this.imageURL);
      },
      (err) => {  
        console.log(err);
      }
    );
  }
  

  imagePreviewEdit(e) {
    const file = (e.target as HTMLInputElement).files[0];

    this.editForm.patchValue({
      img: file
    });

    this.editForm.get('img').updateValueAndValidity()

    
    
      this.url = this.doc.file
    
  
  }


  imagePreview(e) {
    const file = (e.target as HTMLInputElement).files[0];

    this.editForm.patchValue({
      img: file
    });

    this.editForm.get('img').updateValueAndValidity()

    const reader = new FileReader();
    reader.onload = () => {
      this.url = reader.result as string;
    }
    reader.readAsDataURL(file)
  }

  //selectFile(event) { //Angular 8
	selectFile(event :any) { //Angular 11, for stricter type
		if(!event.target.files[0] || event.target.files[0].length == 0) {
			this.msg = 'You must select an image';
			return;
		}
		
		var mimeType = event.target.files[0].type;
		
		if (mimeType.match(/image\/*/) == null) {
			this.msg = "Only images are supported";
			return;
		}
    const file = event.target.files[0];
    this.editForm.get('docImage').setValue(file);

		var reader = new FileReader();
		reader.readAsDataURL(event.target.files[0]);
		
		reader.onload = (_event) => {
			this.msg = "";
			this.url = reader.result;
      //this.img = ("http://127.0.0.1:8000/media/images/" + event.target.files[0].name);
    
		}

	}

}

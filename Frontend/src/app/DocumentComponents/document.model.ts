import { User } from "../auth/auth.model";

export interface createDoc{
    id?:string,
    docImage: File,
    title: string,
    description: string,
    modelType: string,
    owner: string,
}

export interface listDocs{
    id?:string,
    docImage: File,
    title: string,
    description: string,
    modelType: string,
    owner: string,
    creationDate: Date,   
}

export interface listDocss{
    owner : string
}

export class Document{
    constructor(
        public id: string,
        public docImage: File,
        public title: string,
        public description: string,
        public creationDate: Date,
        public modelType: string,
        public owner: User,
        public file: string,
        public ownerMail?: string,

    ){}

    }
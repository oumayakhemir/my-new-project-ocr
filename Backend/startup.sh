#!/bin/sh

while ! nc -z postgresdb 5432 ; do
  echo "Waiting for PostgreSQL"
  sleep 3
done

python manage.py migrate --noinput

python manage.py createsuperuser --noinput

python manage.py runserver 0.0.0.0:8001